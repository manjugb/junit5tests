package codingchallengetests;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.constants.myLogger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import codingchallenges.CodingChallenge;



public class CodingChallengeTests {
	
	CodingChallenge fixture;
		
		@BeforeEach
		void setUp() throws Exception {
			fixture = new CodingChallenge();
			

		}
		
		
		@Test
		@DisplayName("Find Missing Number from sorted list")
		public void testsortInts() {
			final int[] numbers = {0,1,2,3,4,5,7};
			int missing = 6;
			int actmissing = CodingChallenge.findFirst(numbers);
			myLogger.info("Missing Number:"+actmissing);
			assertEquals(missing, actmissing);
		}
		
		@Test
		@DisplayName("Find occurence of Letter")
		public void testgetCount() throws Throwable {
			System.out.println("Find Count");
			char[] s = { 'A', 'B', 'C', 'A', 'B', 'A', 'C', 'A', 'B' };
			//System.out.println(s);
			String str = String.valueOf(s);
			fixture.getCount(str);
			myLogger.info("Found Count");
		}
		
		@Test
		@DisplayName("Convert integer to binary")
		public void testintbinary() {
			String binary = CodingChallenge.inttobinary(10);
			String expbin = "1010";
			assertEquals(expbin,binary);
			
		}

		@AfterEach
		void tearDown() throws Exception {

		}

}
