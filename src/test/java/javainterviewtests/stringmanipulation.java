package javainterviewtests;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javainterview.javainterviewques;
import javainterview.stringmanipulations;

public class stringmanipulation {
	stringmanipulations fixture;
	
	@BeforeEach
	void setUp() throws Exception {
		fixture = new stringmanipulations();
	
	
	}

	@AfterEach
	void tearDown() throws Exception {

	}
	
	@Test
	@DisplayName("String Manupulations")
	public void testStringManipulations() {
		//test length of string
		String s = "Find smallest and Largest Number in array";
		String s1 = "Find smallest and Largest Number in array";
		int len = fixture.findLengthofString(s);
		int explen = 41;
		assertEquals(explen, len);
		//find char in the array
		char expchar = 'd';
		char actchar = fixture.findCharValueOfString(s,3);
		assertEquals(expchar, actchar);
		
		//find the position of char
		
		int exppos = 5;
		char ch = 's';
		int actpos = fixture.findPostionofCharInString(s, ch, s.indexOf(ch)+1);
		assertEquals(exppos, actpos);
		
			
	}
	
	@Test
	@DisplayName("Check Substring in String")
	public void testcheckSubString() throws Throwable{
		String s = "Check Substring of String";
		String expSubSting = "Check";
		String actSubString = fixture.getSubString(s,0,5);
		if(actSubString.equals(expSubSting)){
			assertEquals(expSubSting, actSubString);
		}else {
			assertNotEquals(expSubSting, actSubString);
		}
		
		
	}
	
	//check remove space at start of the string and end of the string
	@Test
	@DisplayName("Check Remove Space")
	public void testTrimOfString() throws Throwable{
		String s = "  Check Remove Space  ";
	    String exps = "CheckRemoveSpace";
	    String acts = fixture.checkTrimString(s, " ", "");
	   
	    System.out.println(s);
	    assertEquals(exps, acts);
	    System.out.println(acts);
	    
	    //Date
	    String date = "01-01-2021";
	    String expdate = "01/01/2021";
	    String actdate = fixture.checkTrimString(date, "-", "/");
	    System.out.println(actdate);
	    assertEquals(expdate, actdate);
	    
	}
	
	//split and dislpay the values
	
	@Test
	@DisplayName("Split and display the values in a String")
	public void testSplitDisplayValues() throws Throwable{
		String s = "Hello_World_Test_Selenium";
		String exps = "Hello World Test Selenium";
		
		String act = fixture.splitString(s, "_");
	
				assertEquals(exps, act);
			
			
		
		
		
	}
	
	
	

}
