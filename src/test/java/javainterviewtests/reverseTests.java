package javainterviewtests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotEquals;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javaMethods.Solution;
import javaMethods.SortTechniques;
import javaMethods.dataStructures;
import javaMethods.swap;
import javainterview.javainterviewques;

public class reverseTests {
	
	javainterviewques fixture;
	
	@BeforeEach
	void setUp() throws Exception {
		fixture = new javainterviewques();
	
	
	}

	@AfterEach
	void tearDown() throws Exception {

	}
	
	@Test
	@DisplayName("Reverse String using ForLoop")
	public void testReverseString() throws Throwable{
		String s = "Manjunath";
		String exps = "htanujnaM";
		String actString = fixture.reverse(s);
		assertEquals(exps, actString, "Matched");
		
		String s1 = "";
		String exps1 = "";
		String actString1 = fixture.reverse(s1);
		assertEquals(exps1, actString1, "Matched");
		
	}
	
	
	@Test
	@DisplayName("Reverse String using Sring Buffer")
	public void testReverseStringBuffer() throws Throwable{
		String s = "Manjunath";
		String exps = "htanujnaM";
		 
		StringBuffer actString = fixture.reverseWithStringBuffer(s);;
		Assert.assertEquals(exps, actString.toString());
		
		String s1 = "";
		String exps1 = "";
		StringBuffer actString1 = fixture.reverseWithStringBuffer(s1);
		assertEquals(exps1, actString1.toString(), "Matched");
		
	}
	
	@Test
	@DisplayName("Remove special Charectors in String")
	public void testRemoveSpecialCharInString() throws Throwable{
		String s = "@@$#$%%   ^Bangalore%$%$^$%不不不不不不不";
		String actStr = fixture.removeSpecialCharInString(s);
		System.out.println(actStr);
		String expStr = "Bangalore";
		assertEquals(expStr, actStr);
	}
	
	@Test
	@DisplayName("Reverse the long or integer number")
    public void testcheckReverseOfNumber() throws Throwable{
		long num = 234567;
		long expnum = 765432;
		long actnum = fixture.checkReverseOfNumber(num);
		assertEquals(expnum, actnum);
	}
	
	@Test
	@DisplayName("Find Missing Number")
	public void testmissingNumber() throws Throwable{
		long[] numbers = {1,2,3,5,6,7,8,9,10};
		long expnum = 4;
		long actnum = fixture.missingNumber(numbers);
		
		assertEquals(expnum, actnum);
		
		
	}
	
	@Test
	@DisplayName("Find Missing Number in n arrays")
	public void testmissingNumberN() throws Throwable{
		int[] numbers = {0,1,2,3,4,5,6,7,8,9,11};
		int expnum = 10;
		int actnum = javainterviewques.findMissingN(numbers);
		
		assertEquals(expnum, actnum);
		
		
	}
	
	@Test
	@DisplayName("Find Duplicates in  arrays")
	public void testduplicateNumber() throws Throwable{
		String names[] = {"sony","samsung","sony","samsung"};
		String expString = "sony";
		String actString = javainterviewques.findDuplicates(names);
		assertEquals(expString, actString, "matched");
		String names1[] = {"sony","samsung","lib","ding"};
		String actString1 = javainterviewques.findDuplicates(names1);
		assertNotEquals(expString, actString1, "not matched");
		
	}
	
	@Test
	@DisplayName("Find Duplicates in  arrays using HashSet")
	public void testduplicateNumberHashSet() throws Throwable{
		String names[] = {"sony","samsung","sony","samsung"};
		String expString = "sony";
		String expString1 = "samsung";
		String actString = javainterviewques.findDuplicatesHashSets(names);
		System.out.println(actString);
		assertEquals(expString, actString, "matched");
		
	}
	
	
	@Test
	@DisplayName("Find Duplicates in  arrays using HashMap")
	public void testduplicateNumberHashMap() throws Throwable{
		String names[] = {"sonyHashMap","samsung","sonyHashMap","samsung"};
		String expString = "sonyHashMap";
		String actString = javainterviewques.findDuplicatesHashMap(names);
		System.out.println(actString);
		assertEquals(expString, actString, "matched");
		
	}
	
	@Test
	@DisplayName("Find smallest and Largest Number in array")
	public void testfindlargestSmallestNumbers() throws Throwable{
		int numbers[] = {-10,24,50,-88,98765};
		int[] expnumbers = {98765,-88};
		int[] actnumbers = fixture.smallLargestNuber(numbers);
	
		assertArrayEquals(expnumbers, actnumbers);
	}

	@Test
	@DisplayName("Find duplicate character in a String")
	public void testfind_duplicate_cahr() throws Throwable{
		String s = "Manjunath";
		String exps = "M 1 Times\n" + "a 2 Times\n" + "n 2 Times\n" + "j 1 Times\n" + "u 1 Times\n" + "t 1 Times\n" + "h 1 Times";
		javainterviewques.findDuplicate2_char(s);
		String s1 = javainterviewques.findDuplicate2_char(s);
		System.out.println(s1);
	    assertEquals(exps, s1);
	}
	
	@Test
	@DisplayName("Find duplicate character in a String using hash map")
	public void testfind_duplicate_cahr_hashmap() throws Throwable{
		String s = "Manjunath";
		
		javainterviewques.findDuplicate2_char(s);
		Map<Character, Integer> s1 = javainterviewques.finduplicatechar(s);
		System.out.println(s1);
	    //assertEquals(s1.containsKey(s1));
	}
	
    
}
