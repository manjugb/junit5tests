package momentoTest;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import momentopattern.Editor;
import momentopattern.History;


@DisplayName("Content Editor Unit Testing")
public class editorUnitTest {
	

	Editor fixture;

	@BeforeEach
	void setUp() throws Exception {
		fixture = new Editor();

	}

	@AfterEach
	void tearDown() throws Exception {

	}
	
	@Test
	@DisplayName("Set Content Validation")
	public void testsetContent_one() {
		Editor editor = new Editor();
		History history = new History();
					
		String expected = "Hi";
		//String actual = "Hi";
	    fixture.setContent("Hi");
	    String strgetContent = fixture.getContent();
	    history.push(editor.createState());
	    Assert.assertEquals("Content Matched",expected,strgetContent);
	    //System.out.println(strgetContent);
	    
	    
	}
	
	
	
	
	@Test
	@DisplayName("Set Content Validation")
	public void testsetContent_two() {
		String expected = "Hi";
		fixture.setContent("Explorative Testing");
	    String strgetContent = fixture.getContent();
	    Assert.assertNotEquals("Content Matched",expected,strgetContent);
	    System.out.println(strgetContent);
	    
	}
	
	@Test
	@DisplayName("Set Content Validation")
	public void testSetContent_isEmpty_one() {
		String expected = null;
		
	    fixture.setContent("");
	    String strgetContent = fixture.getContent();
	    Assert.assertEquals(expected,strgetContent);
	    
	}
	
	@Test
	@DisplayName("Set Content Validation")
	public void testSetContent_isEmpty_two() {
		String expected = null;
		
	    fixture.setContent("Content Testing");
	    String strgetContent = fixture.getContent();
	    Assert.assertNotEquals(expected,strgetContent);
	    
	}
}
