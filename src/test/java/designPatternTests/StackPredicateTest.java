package designPatternTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import designPatterns.Stack;
import designPatterns.StackPredicate;

public class StackPredicateTest {
	private Stack stack;

	@BeforeEach
	public void testcreateStack() throws Throwable {
		stack = new Stack();
		for (int i = 1; i <= 10; i++) {
			stack.push(i);
		}
	}

	@Test
	@DisplayName("Predicate")
	public void testevenPredicate() throws Throwable{
		final Stack filtered = stack.filter(new StackPredicate() {
			@Override
			public boolean isValid(int i) {
				return (i % 2 == 0);
			}
		});
		assertEquals(Integer.valueOf(10), filtered.pop());
		assertEquals(Integer.valueOf(8), filtered.pop());
		assertEquals(Integer.valueOf(6), filtered.pop());
		assertNotEquals(Integer.valueOf(12), filtered.pop());
		assertNotEquals(Integer.valueOf(-2), filtered.pop());
	
	}

	@Test
	@DisplayName("All Predicate")
	public void allPredicate() throws Throwable {
		final Stack filtered = stack.filter(new StackPredicate() {
			@Override
			public boolean isValid(int i) {
				return true;
			}
		});
		assertEquals(Integer.valueOf(10), filtered.pop());
		assertEquals(Integer.valueOf(9), filtered.pop());
		assertEquals(Integer.valueOf(8), filtered.pop());
	}
}