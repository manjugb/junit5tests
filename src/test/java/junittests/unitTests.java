package junittests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

import org.junit.jupiter.api.AfterEach;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javaMethods.ReverseNumericalOrder;
import javaMethods.Solution;
import javaMethods.SortTechniques;
import javaMethods.dataStructures;
import javaMethods.swap;
import junit.framework.Assert;
import mypractisedemo.Main;
import mypractisedemo.User;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static util.constants.myLogger;

@DisplayName("Swaping two Numbers")
class unitTests {
	swap fixture;
	SortTechniques fixture1;
	dataStructures fixture2;
	Solution fixture3;
	User fixture4;

	@BeforeEach
	void setUp() throws Exception {
		fixture = new swap();
		fixture1 = new SortTechniques();
		fixture2 = new dataStructures();
		fixture3 = new Solution();

	}

	@AfterEach
	void tearDown() throws Exception {

	}

	@Test
	@DisplayName("test of swapnig numbers with out temp variable")
	void testswapwithtemp() throws Throwable {

		fixture.swaptemp(4, 5);

	}

	/*
	 * assertAll( () -> assertEquals(firstName, person.getFirstName()), () ->
	 * assertEquals(lastName, person.getLastName()), () -> assertEquals(age,
	 * person.getAge()), () -> assertEquals(eyeColor.toString(),
	 * person.getEyeColor()), () -> assertEquals(gender.toString(),
	 * person.getGender())); }
	 * 
	 */
	@Test
	@DisplayName("Array Sorting")
	public void sortInts() {
		final int[] numbers = { -3, -5, 1, 7, 4, -2 };
		final int[] expected = { -5, -3, -2, 1, 4, 7 };
		Arrays.sort(numbers);
		assertArrayEquals(expected, numbers);
	}

	@Test
	@DisplayName("Sorting Strings")
	public void sortObjects() {

		final String[] strings = { "z", "x", "y", "abc", "zzz", "zazzy" };
		final String[] expected = { "abc", "x", "y", "z", "zazzy", "zzz" };
		Arrays.sort(strings);
		assertArrayEquals(expected, strings);

	}

	@Test
	@DisplayName("Custom Soring")
	public void customSorting() {
		final List<Integer> numbers = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
		final List<Integer> expected = Arrays.asList(7, 6, 5, 4, 4, 3, 1);
		Collections.sort(numbers, new ReverseNumericalOrder());
		assertEquals(expected, numbers);
	}

	@Test
	@DisplayName("Bubble Soring")
	public void testbubbleSort() throws Throwable {

		int arr[] = { 2, 5, -2, 6, -3, 8, 0, -7, -9, 4 };
		myLogger.info("Array Before Bubble Sort");

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		//System.out.println();
		fixture1.bubbleSort(arr);
		myLogger.info("Array After Bubble Sort");

		for (int i = 0; i < arr.length; i++) {
			myLogger.info(arr[i] + " ");
		}

	}

	
	@Test
	@DisplayName("Insertion Sorting for Integer Values")
	public void testInsertionSort() {

		List<Integer> list = asList(4, 6, 8, 10, 6, 4, 0, 1, 2, 11);
		List<Integer> sorted = asList(0, 1, 2, 4, 4, 6, 6, 8, 10, 11);
		List<Integer> actuals = fixture1.insertSort(list);
		myLogger.info("Display Values After Sorting :" + actuals);

		assertEquals(actuals, sorted);

	}

	@Test
	@DisplayName("Quick Sort for Integer Values")
	public void testQuickSort() {

		List<Integer> list = asList(0, 6, 3, 4, 1, -2);
		List<Integer> sorted = asList(-2, 0, 1, 3, 4, 6);
		List<Integer> actuals = fixture1.quicksort(list);
		myLogger.info("Display Values After Sorting :" + actuals);

		assertEquals(actuals, sorted);

	}

	@Test
	@DisplayName("Bubble Soring")
	public void testbubbleSort2() throws Throwable {

		int[] arr = { 2, 5, -2, 6, -3, 8, 0, -7, -9, 4 };
		int[] exp = { -9, -7, -3, -2, 0, 2, 4, 5, 6, 8 };
		int[] act = fixture1.bubbleSort1(arr);
		myLogger.info("Array Before Bubble Sort");

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		//System.out.println("Array After Bubble Sort");

		for (int i = 0; i < act.length; i++) {
			System.out.print(act[i] + " ");

		}
		assertArrayEquals(exp, act);

	}

	@Test
	@DisplayName("Bubble Soring By Comaparing Adjacent items")
	public void testbubbleSort4() throws Throwable {

		int[] arr = { 29, 10, 14, 37, 13 };
		int[] exp = { 10, 13, 14, 29, 37 };
		int[] act = fixture1.bubbleSort2(arr);
		//System.out.println("Array Before Bubble Sort");

		for (int i = 1; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		//System.out.println("Array After Bubble Sort");

		for (int i = 1; i < act.length; i++) {
			System.out.print(act[i] + " ");

		}
		assertArrayEquals(exp, act);

	}

	@Test
	@DisplayName("Bubble Soring")
	public void testbubbleSort3() throws Throwable {

		int[] arr = { 1, 2, 3, 4, 5 };
		int[] exp = { 1, 2, 3, 4, 5 };
		int[] act = fixture1.bubbleSort1(arr);
		//System.out.println("Array Before Bubble Sort");

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		//System.out.println("Array After Bubble Sort");
		assertArrayEquals(exp, act);
		for (int i = 0; i < act.length; i++) {
			System.out.print(act[i] + " ");

		}

	}

	@Test
	@DisplayName("Merge Sort for Integer Values")
	public void testMergSort() {

		List<Integer> list = asList(0, 6, 3, 4, 1, 3);
		List<Integer> sorted = asList(0, 1, 3, 3, 4, 6);
		List<Integer> actuals = fixture1.mergesort(list);
		//System.out.println("Display Values After Sorting :" + actuals);

		assertEquals(actuals, sorted);

	}

	@Test
	@DisplayName("Binary Search Element Not Found ")
	public void testBinarySearch_notfound() {

		final List<Integer> numbers = Arrays.asList(1, 3, 4, 5, 6, 7);
		boolean actvalue = fixture1.binarySearch(numbers, 10);
		boolean actvalue1 = fixture1.binarySearch(numbers, 4);
		boolean exp = true;
		assertNotEquals(exp, actvalue);
		assertEquals(exp, actvalue1);

	}
	
	
	
	

	@Test
	@DisplayName("Binary Search Element Found ")
	public void testBinarySearch_found() {

		final List<Integer> numbers = Arrays.asList(1, 3, 4, 4, 5, 6, 7);
		boolean actvalue = fixture1.binarySearch(numbers, 1);
		boolean exp = true;
		assertEquals(exp, actvalue);
	}

	@Test
	@DisplayName("Binary Search is Empty ")
	public void testBinarySearch_empty() {
		final List<Integer> numbers = null;
		boolean actvalue = fixture1.binarySearch(numbers, 0);
		boolean exp = false;
		assertEquals(exp, actvalue);
	}

	@Test
	@DisplayName("Que Insertion Using linkedlist")
	public void testqueueInsertion() {
		final Queue<String> queue = new LinkedList<>();
		queue.add("first");
		queue.add("second");
		queue.add("third");
		assertEquals("first", queue.remove());
		assertEquals("second", queue.remove());
		assertEquals("third", queue.peek());
		assertEquals("third", queue.remove());

	}
	/*
	 * A map, sometimes called a hash, associative array or dictionary, is a
	 * key-value store. Elements within the data structure can be queried by the
	 * key, which will return the associated value. The Map interface is part of the
	 * Java Collections API, but, unlike List , it does not implement the Collection
	 * interface. Similar to the List interface, the Map interface specifies most
	 * common
	 * 
	 */

	@Test
	@DisplayName("Maps Example")
	public void testoverwriteKey() {
		final Map<String, String> preferences = new HashMap<>();
		preferences.put("like", "jacuzzi");
		preferences.put("dislike", "steam room");
		assertEquals("jacuzzi", preferences.get("like"));
		preferences.put("like", "sauna");
		assertEquals("sauna", preferences.get("like"));
	}

	@Test
	public void treeMapTraversal() {
		final Map<Integer, String> counts = new TreeMap<>();
		counts.put(4, "four");
		counts.put(1, "one");
		counts.put(3, "three");
		counts.put(2, "two");
		final Iterator<Integer> keys = counts.keySet().iterator();
		assertEquals(Integer.valueOf(1), keys.next());
		assertEquals(Integer.valueOf(2), keys.next());
		assertEquals(Integer.valueOf(3), keys.next());
		assertEquals(Integer.valueOf(4), keys.next());
		assertFalse(keys.hasNext());
	}
	/*
	 * A set is an unordered collection of objects that does not contain any
	 * duplicates. The Java Collections API contains a Set interface, extending from
	 * Collection , which provides many methods for inspecting and modifying a set
	 */

	@Test
	@DisplayName("Set Example")
	public void setExample() {
		final Set<String> set = new HashSet<>();
		set.add("hello");
		set.add("welcome");
		set.add("goodbye");
		set.add("bye");
		set.add("hello");
		set.add("UnitTesting");
		assertNotEquals(4, set.size());
		assertEquals(5, set.size());
		assertEquals(4, 4);
	}

	@Test
	@DisplayName("Factorial Using Loop")
	public void testFactorialLoop() {
		fixture2.factorialUsingForLoop(10);
	}

	@Test
	@DisplayName("Factorial Using Loop")
	public void testFactorialLoop1() {
		fixture2.factorialUsingForLoop(20);
	}

	@Test
	@DisplayName("Factorial Stream API")
	public void testFactorialApi() {
		fixture2.factorialUsingStreams(3);
	}

	@Test
	@DisplayName("Factorial Recursion")
	public void testFactorialrecurs() {
		fixture2.factorialUsingRecursion(4);
		fixture2.factorialUsingRecursion(1);
		fixture2.factorialUsingRecursion(2);
	}

	@Test
	@DisplayName("Summation")
	public void testSummation() {
		fixture2.Summation(3);
		// fixture2.Summation(0);
	}

	@Test
	@DisplayName("ntimesk")
	public void testntimesk() throws Throwable {
		int result = fixture2.nTimesK(4, 2);
		int result1 = fixture2.nTimesK(4, 4);
		int result2 = fixture2.nTimesK(1, 1);
		Assert.assertEquals(8, result);
		Assert.assertNotSame(7, result);
		Assert.assertEquals(16, result1);
		Assert.assertEquals(1, result2);

		// fixture2.nTimesK(2, 2);
	}

	@Test
	@DisplayName("SelectionSort Example smallest item")
	public void givenUnsortedArray_whenInsertionSortImperative_thenSortedAsc() {
		int[] input = { 6, 2, 3, 4, 5, 1 };
		System.out.println("Array Before Selection Sort");
		for (int i = 0; i < input.length; i++) {
			System.out.print(input[i] + " ");
		}
		int[] actual = fixture1.sortAscending(input);
		// System.out.println("From Execution" actual);
		int[] expected = { 1, 2, 3, 4, 5, 6 };

		System.out.println();

		System.out.println("Array After Selection Sort");
		assertArrayEquals("the two arrays are equal", expected, actual);
		for (int i = 0; i < actual.length; i++) {
			System.out.print(actual[i] + " ");

		}
	}

	@Test
	@DisplayName("SelectionSort Example from largest item item")
	public void givenUnsortedArray_whenInsertionSortRecursive_thenSortedAsc() {
		int[] input = { 6, 4, 5, 2, 3, 1 };
		fixture1.sortAscending(input);
		int[] expected = { 1, 2, 3, 4, 5, 6 };
		assertArrayEquals("the two arrays are not equal", expected, input);
	}

	@Test
	@DisplayName("Find occurence of Letter")
	public void testgetCount() throws Throwable {
		System.out.println("Find Count");
		char[] s = { 'A', 'B', 'C', 'A', 'B', 'A', 'C', 'A', 'B' };
		System.out.println(s);
		String str = String.valueOf(s);
		fixture.getCount(str);
		System.out.println("Found Count");
	}

	@Test
	@DisplayName("Find occurence of Duplicates")
	public void testgetduplicates() throws Throwable {
		System.out.println("Find Count");
		char[] s = { 'A', 'B', 'C', 'A', 'B', 'A', 'C', 'A', 'B' };
		System.out.println(s);
		String str = String.valueOf(s);
		fixture.findDuplicate(str);
		System.out.println("Found Count");
	}
	
	@Test
	@DisplayName("InsertionSort_shiftright")
	public void testInsertionSort2() throws Throwable {
		int[] arr = {8,2,4,1,3};
		//for(int i=0;i<arr.length;i++)
	
		fixture1.InsertionSort_shiftright(arr);
		System.out.println("Sorted "+arr+ "Array");
	}
	
	


}
