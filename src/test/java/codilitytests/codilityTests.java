package codilitytests;

import static util.constants.myLogger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class codilityTests {
	Codility fixture;

	@BeforeEach
	void setUp() throws Exception {
		fixture = new Codility();

	}

	@AfterEach
	void tearDown() throws Exception {

	}

	@Test
	@DisplayName("Find value that occurs in add number of elements")
	public void testoddoccrnces() throws Throwable {
		int arr[] = new int[] { 9, 3, 9, 3, 9, 7, 9 };
		int n = arr.length;
		myLogger.info(Codility.getOddOccurrence(arr, n));

	}

	@Test
	@DisplayName("Find value that occurs in add number of elements")
	public void testoddoccrnces1() throws Throwable {
		int arr[] = new int[] { 9, 3, 9, 3, 9, 7, 9 };
		int n = arr.length;
		myLogger.info(Codility.getOddOccurrence1(arr, n));

	}

	@Test
	@DisplayName("Find value that occurs in add number of elements")
	public void testoddoccrnces2() throws Throwable {
		int arr[] = new int[] { 9, 3, 9, 3, 9, 6, 9 };
		int n = arr.length;
		myLogger.info(Codility.getOddOccurrence2(arr, n));
	}

	@Test
	@DisplayName("Cyclic Roation")
	public void testCyclicRotation() throws Throwable {
		// Initialize array
		int[] arr = new int[] { 1, 2, 3, 4, 5 };
		// n determine the number of times an array should be rotated.
		int n = 3;
		Codility.cyclicrotation(arr, n);
	}

	@Test
	@DisplayName("frogjump count number of jumps from X to Y")
	public void testfrogjump() throws Throwable {
		fixture.frogjump(10, 90, 30);
	}
}
