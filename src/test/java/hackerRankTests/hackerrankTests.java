package hackerRankTests;

import static org.junit.Assert.assertArrayEquals;
import static util.constants.myLogger;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javaMethods.dataStructures;

import hackerrank.meetingSchedule;

import hackerrank.transactions;

class hackerrankTests {
	meetingSchedule fixture;
	transactions fixture1;
	dataStructures fixture2;

	

	@BeforeEach
	void setUp() throws Exception {
		fixture = new meetingSchedule();
		fixture1 = new transactions();
		fixture2 = new dataStructures();
		

	}

	@AfterEach
	void tearDown() throws Exception {

	}

	// @Test
	@DisplayName("HackerRank")
	public void testMeetingSchedule() {
		List<Integer> firstday = Arrays.asList(1, 1, 2);
		List<Integer> lastday = Arrays.asList(1, 2, 2);
		List<Integer> firstday1 = Arrays.asList(1, 2, 1, 2, 2);
		List<Integer> lastday1 = Arrays.asList(3, 2, 1, 3, 3);
		meetingSchedule.getMinMeetings(firstday, lastday);
		meetingSchedule.getMinMeetings(firstday1, lastday1);

		//System.out.println("Schedule Meeting Start");
		//System.out.println(meetingSchedule.getMinMeetings(firstday, lastday));
		//System.out.println(meetingSchedule.getMinMeetings(firstday1, lastday1));
		/*
		 * System.out.println(meetingSchedule.getMinMeetings(arrival2, departure2));
		 * System.out.println(meetingSchedule.getMinMeetings(arrival3, departure3));
		 * System.out.println(meetingSchedule.getMinMeetings(arrival4, departure4));
		 * System.out.println(meetingSchedule.getMinMeetings(arrival5, departure5));
		 * System.out.println(meetingSchedule.getMinMeetings(arrival6, departure6));
		 */
		//System.out.println("Schedule Meeting End");
	}

//@Test
	@DisplayName("getTranscations")
	public void testGetTransactions() throws Throwable {

		transactions.getTransactions(2, 8, 5, 50);
		// transactions.getTransactions(1,8,5,50);

	}

	@DisplayName("Movie Titels")
	public void testmovietitles() throws Throwable {
		// transactions.getMovieTitles1("Test");

		// String keyword = "Spiderman";
		int retAmount;

		retAmount =  transactions.getTransactions(1, 8, 5, 50);

		//System.out.println(retAmount);
	}

	

	@SuppressWarnings("deprecation")
	@Test
	@DisplayName("DictionaryPractice")
	public void testDictionaryPractice() throws Throwable {
		Map<String, String> s = new HashMap<String, String>();
		s.put("Monday", "Lundi");
		s.put("Thuesday", "Mardi");
		s.put("Wednseday", "Mercredi");
		s.put("Thursday", "Jeudi");

		@SuppressWarnings("unused")
		Map<String, String> s2 = dataStructures.getDictionary(s);
		Assert.assertEquals("Lundi", s.get("Monday"));
		Assert.assertEquals("Mardi", s.get("Thuesday"));
		Assert.assertEquals("Mercredi", s.get("Wednseday"));
		Assert.assertEquals("Jeudi", s.get("Thursday"));
		// Assert.assertEquals("Lundia", s.get("Monday"));

		/*
		 * s.forEach((k, v) -> { System.out.format("key: %s, value: %d%n", k, v); });
		 */

		Iterator<String> it = s.keySet().iterator();

		while (it.hasNext()) {
			String key = it.next();

			Set<String> keys = s.keySet();
			Collection<String> Values = s.values();

			keys.forEach(System.out::println);
			Values.forEach(System.out::println);

		}

	}

	@Test
	@DisplayName("DictionaryPractice - Strings ListArrays")
	public void testgetListMaps() throws Throwable {
		Map<String, List<String>> m = new HashMap<String, List<String>>();
		m.put("colours", Arrays.asList("red", "green", "blue"));
		m.put("sizes", Arrays.asList("small", "medium", "big"));
		m.put("Countries", Arrays.asList("India", "Portugal", "Canada", "Belgium", "Germany", "Malaysia"));

		for (Map.Entry<String, List<String>> me : m.entrySet()) {

			String key = me.getKey();
			List<String> values = me.getValue();

			myLogger.info("Key: " + key);
			myLogger.info("Values: ");

			for (String e : values) {

				System.out.printf("%s ", e);
			}

			//System.out.println();
			assertArrayEquals(key, values);
		}
	}

	private boolean assertArrayEquals(String key, List<String> values) {
		// TODO Auto-generated method stub
		return true;
	}

	private boolean assertArrayEquals(String key, Boolean values) {
		// TODO Auto-generated method stub
		return false;
	}

	

}
