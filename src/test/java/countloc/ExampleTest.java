package countloc;

import java.io.*;
import java.nio.file.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class ExampleTest {
    @Test
    public void shouldHandleBasicCode() throws IOException {
        String path = "/home/manjunath/jenkins/junitTDD/src/test/resources/count/Example1.java";
        String code = new String(Files.readAllBytes(Paths.get(path)));
        assertEquals(6, CountLOC.count(code));
    }
  
    @Test
    public void shouldHandleABlankLineWIthOneLineComment() throws IOException {
        String path = "/home/manjunath/jenkins/junitTDD/src/test/resources/count/Example2.java";
        String code = new String(Files.readAllBytes(Paths.get(path)));
        assertEquals(6, CountLOC.count(code));
    }
  
    @Test
    public void shouldHandleAnInlineComment() throws IOException {
        String path = "/home/manjunath/jenkins/junitTDD/src/test/resources/count/Example3.java";
        String code = new String(Files.readAllBytes(Paths.get(path)));
        assertEquals(6, CountLOC.count(code));
    }
  
    @Test
    public void shouldHandleMultilineCommentsAndQuotes() throws IOException {
        String path = "/home/manjunath/jenkins/junitTDD/src/test/resources/count/Example4.java";
        String code = new String(Files.readAllBytes(Paths.get(path)));
        assertEquals(6, CountLOC.count(code));
    }
  
    @Test
    public void shouldHandleAComplexExample() throws IOException {
        String path = "/home/manjunath/jenkins/junitTDD/src/test/resources/count/Example5.java";
        String code = new String(Files.readAllBytes(Paths.get(path)));
        assertEquals(4, CountLOC.count(code));
    }
}