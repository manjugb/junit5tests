package countloc;

import java.io.*;
import java.nio.file.*;
import org.junit.Test;
import static org.junit.Assert.*;
import static util.constants.myLogger;

public class CandidateTest {
    @Test
    public void shouldWorkOnYourCustomTest() throws IOException {
      
        /* This file will not be submitted--feel free to 
           experiment and write tests here as you wish. */
      
        String path = "/home/manjunath/jenkins/junitTDD/src/test/resources/count/Example1.java";
        String code = new String(Files.readAllBytes(Paths.get(path)));
        myLogger.info(code);
        assertEquals(6, CountLOC.count(code));
    }
}