package codilityTests;

import static util.constants.myLogger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class codilityTests {
	codility fixture;
	

	@BeforeEach
	void setUp() throws Exception {
		fixture = new codility();
		
	}
	

	@AfterEach
	void tearDown() throws Exception {

	}
	/*@Test
	@DisplayName("test Find Largest sequence of zeros in binary representation of an integer")
	public void testbingap() throws Throwable{
		fixture.solution(93);
		
	}*/
    @Test
    @DisplayName("Find value that occurs in add number of elements")
    public void testoddoccrnces() throws Throwable{
    	int arr[] = new int[]{ 9,3,9,3,9,7,9 }; 
        int n = arr.length; 
        myLogger.info(fixture.getOddOccurrence(arr, n)); 
    	
    }
    
    @Test
    @DisplayName("Find value that occurs in add number of elements")
    public void testoddoccrnces1() throws Throwable{
    	int arr[] = new int[]{ 9,3,9,3,9,7,9 }; 
        int n = arr.length; 
        myLogger.info(fixture.getOddOccurrence1(arr, n)); 
    	
    }
    
    @Test
    @DisplayName("Find value that occurs in add number of elements")
    public void testoddoccrnces2() throws Throwable{
    	int arr[] = new int[]{ 9,3,9,3,9,6,9 }; 
        int n = arr.length; 
        myLogger.info(fixture.getOddOccurrence2(arr, n)); 
    }
        
    @Test
    @DisplayName("Cyclic Roation")
    public void testCyclicRotation() throws Throwable{
        //Initialize array     
        int [] arr = new int [] {1, 2, 3, 4, 5};     
        //n determine the number of times an array should be rotated.    
        int n = 3;    
        fixture.cyclicrotation(arr, n);   
        }     
    @Test
    @DisplayName("frogjump count number of jumps from X to Y")
    public void testfrogjump() throws Throwable{
    	fixture.frogjump(10, 90, 30);
    }
    }


