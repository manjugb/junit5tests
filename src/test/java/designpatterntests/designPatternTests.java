package designpatterntests;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.constants.myLogger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import designpatterns.BookType;
import designpatterns.LibraryBook;

public class designPatternTests {

	@Test
	@DisplayName("Design Pattern Builder Example")
	public void fictionLibraryBook() {
		final LibraryBook.Builder builder = new LibraryBook.Builder();
		final LibraryBook book = builder.withBookName("War and Peace").build();
		myLogger.info(book.getBookName());
		//assertTrue(book.getBookName().toString());
		assertEquals(BookType.FICTION, book.getBookType());

	}
	/*
	 * The Flyweight Pattern can be a useful pattern in situations where you have
	 * several objects, and many may represent the same value. In these instances,
	 * it can be possible to share the values as long as the objects are immutable
	 */

	@Test
	public void sameIntegerInstances() {
		final Integer a = Integer.valueOf(56);
		final Integer b = Integer.valueOf(56);
		assertSame(a, b);
		final Integer c = Integer.valueOf(472);
		final Integer d = Integer.valueOf(472);
		assertNotSame(c, d);
	}

}
