package mypractisedemo;



public class User {
	/**
	 * 
	 * @author manjunath 
	 * {@summary} user creates name,age attributes.
	 */

	public String name;
	public int age;

	public User(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public void sayHello() {
		/**
		 * @author manjunath
		 * {@summary} display name and age.
		 */
		System.out.println("Hi,my name is" + name + "");
		System.out.println("And,my age is" + age + "");

	}

}
