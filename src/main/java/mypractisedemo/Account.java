package mypractisedemo;

public class Account {
	/**
	 * @author manjunath 
	 * {@summary} this class defined methods call
	 *  getbalance,deposit,withdraw.
	 */
	private float balance;

	public float getBalance() {
		return balance;
	}

	public void deposit(float amount) {
		// this.balance = this.balance + amount; //traditional way
		if (amount > 0) {
			balance += amount;
		}
	}

	public void withdraw(float amount) {
		if (amount > 0) {
			balance -= amount;
		}
	}

}
