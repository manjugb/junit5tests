package mypractisedemo;

public class Main {
	/**
	 * @author manjunath
	 * @param args
	 * {@summary} this main method call user,calculator
	 * account information.
	 */
	public static void main(String[] args) {
		System.out.println("Class Implementatin Starts  ....");
		User user = new User("Sonam", 20);
		user.sayHello();
		System.out.println("Class Implementatin Ends  ....");
		// loose coupling
		System.out.println("Loose Coupling Implementatin Starts  ....");
		TaxCalculator calculator = getCalculator();
		calculator.calculateTax();
		System.out.println("Loose Coupling Implementatin Ends  ....");

		// Encapsulation
		System.out.println("Encapsulation Implementatin Starts  ....");

		Account account = new Account();
		account.deposit(50);
		account.withdraw(10);
		System.out.println(account.getBalance());
		System.out.println("Encapsulation Implementatin Ends  ....");
		// Abstraction
		System.out.println("Abstractions Implementatin Starts  ....");
		MailService mailService = new MailService();
		mailService.sendMail();
		System.out.println("Abstractions Implementatin Ends  ....");

		// Inheritance
		TextBox textBox = new TextBox();
		textBox.eable();

		// Abstraction
		drawUIControl(new TextBox());
		drawUIControl(new CheckBox());
	}

	public static TaxCalculator getCalculator() {
		return new TaxCalculator2019();

	}

	public static void drawUIControl(UIControl control) {
		control.draw();
	}

}
