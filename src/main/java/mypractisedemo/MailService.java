package mypractisedemo;

public class MailService {
	/**
	 * @author manjunath
	 * {@summary} this mail service call other methods.
	 **/
	

	public void sendMail() {
		connect();
		disconnect();
		authenticate();

	}

	private void connect() {
		System.out.println("Connect");
	}

	private void disconnect() {
		System.out.println("Disconnect");
	}

	private void authenticate() {
		System.out.println("Authenticate");
	}

}
