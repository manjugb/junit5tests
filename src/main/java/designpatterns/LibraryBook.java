package designpatterns;

public class LibraryBook {
public static class Builder {
private BookType bookType = BookType.FICTION;
private String bookName;
public Builder withBookType(final BookType bookType) {
this.bookType = bookType;
return this;
}
public Builder withBookName(final String bookName) {
this.bookName = bookName;
return this;
}
public LibraryBook build() {
return new LibraryBook(bookType, bookName);
}
}
private final BookType bookType;
private final String bookName;
public LibraryBook(final BookType bookType, final String bookName) {
this.bookType = bookType;
this.bookName = bookName;
}
public Object getBookType() {
	// TODO Auto-generated method stub
	return bookType;
}
public Object getBookName() {
	// TODO Auto-generated method stub
	return bookName;
}
public LibraryBook build() {
	if (bookType == null || bookName == null) {
			throw new IllegalStateException("Cannot create LibraryBook");
			}
			return new LibraryBook(bookType,bookName);
			}
			
}

