package designpatterns;
/*
 * Example of Template Pattern
 * Common behavior can be defined in a superclass, and then specific variants are written in a sub-
class
 */
import java.util.LinkedList;

public class Stack {
	private final LinkedList<Integer> stack;
    //Intialize LinkedinList
	public Stack() {
		stack = new LinkedList<>();
	}
   //Create intialState
	public Stack(final LinkedList<Integer> initialState) {
		this.stack = initialState;
	}
    //push
	public void push(final int number) {
		stack.add(0, number);
	}
    //pop remove
	public Integer pop() {
		return stack.remove(0);
	}
    //filter it
	public Stack filter(final StackPredicate filter) {
		final LinkedList<Integer> initialState = new LinkedList<>();
		for (Integer integer : stack) {
			if (filter.isValid(integer)) {
				initialState.add(integer);
			}
		}
		return new Stack(initialState);
	}
}

