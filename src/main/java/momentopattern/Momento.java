package momentopattern;

public class Momento {
	public static void main(String[] args) {
		Editor editor = new Editor();
		History history = new History();
		editor.setContent("Hello");
		history.push(editor.createState());
		editor.setContent("Hello World");
		history.push(editor.createState());
		editor.setContent("Hello India");
		editor.restore(history.pop());
		editor.restore(history.pop());
	    System.out.println(editor.getContent());
	}

}
