package momentopattern;

public class Editor {
	private String content;
	
	public EditorState createState() {
		
		return new EditorState(content);
	}
	
	public void restore(EditorState state) {
		
		content = state.getContent();
		
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		if (content!=null && content.length()  > 0) {
		this.content = content;}
		else if (content.isEmpty()){
			System.out.println("Content is an Empty");
			
		}
	}
	
	

}
