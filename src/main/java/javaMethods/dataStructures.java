package javaMethods;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.LongStream;

public class dataStructures {

	public static Map<String, String> getDictionary(Map<String, String> s) {
		s = new HashMap<String, String>();
		return s;

	}

	public static Map<String, List<String>> getListMaps(Map<String, List<String>> m) {

		return m = new HashMap<>();

	}

	/*
	 * The below solution will work fine for numbers up to 20. But, if we try
	 * something bigger than 20, then it will fail because results would be too
	 * large to be fit into a long, causing an overflow.
	 */

	public long factorialUsingForLoop(int n) {
		long fact = 1;
		for (int i = 2; i <= n; i++) {
			fact = fact * i;
			System.out.println("Factorial using Loop:" + fact);
		}
		return fact;
	}

	// Java 8 API Stream
	public long factorialUsingStreams(int n) {
		return LongStream.rangeClosed(1, n).reduce(1, (long x, long y) -> x * y);
	}
	// using recursive

	public long factorialUsingRecursion(int n) {
		//base case
		if (n <= 1) {
			return 1;
		//recursive
		} else {
			long fact = n * factorialUsingRecursion(n - 1);
			System.out.println("Factorial using Recursion:" + fact);
			return fact;

		}
	}

	public int Summation(int n) {
		int sum = 0;
		if (n <= 0) {
			return 0;
		} else {
			sum = n + Summation(n - 1);
			System.out.println("n:"+n+ "" +" Summation: "+sum);
			return n + Summation(n - 1);
			
		}
	}

		
//multiply the using two numbers
	// Multiply 'n' by 'k' using addition:
	public int nTimesK(int n, int k) {
	    System.out.println("n: " + n);
	    // Recursive Case
	    if(n > 1) { 
	        return k + nTimesK(n - 1, k);
	    }
	    // Base Case n = 1
	    else { 
	        return k;
	    }
	}
}
		


