package javaMethods;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
 * author@:Manjunath
 * Description:
 * 6, 4, 9, 5 -> 4, 6, 9, 5: When i = 0, the numbers 6 and 4 are switched
4, 6, 9, 5 -> 4, 6, 5, 9: When i = 2, the numbers 9 and 5 are switched
 */
/*
 * Although this implementation is simple, it is extremely inefficient. The worst case, when you want
to sort a list that is already sorted in reverse order, is a performance of O(n 2 ) : For each iteration, you
are only switching one element. The best case is when a list is already sorted: You make one pass
through the list, and because you have not switched any elements, you can stop. This has a perfor-
mance of O(n) .
 */

public class SortTechniques {
	public static void bubbleSort(int[] arr) {
		int n = arr.length;
		int temp = 0;

		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - i); j++) {
				if (arr[j - 1] > arr[j]) {
					temp = arr[j - 1];
					arr[j - 1] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}

	public int[] bubbleSort1(int[] numbers) {
		// Number is switched next position or not true or false
		boolean numbersSwitched;
		do {
			numbersSwitched = false;
			for (int i = 0; i < numbers.length - 1; i++) {
				// number + 1 < number
				if (numbers[i + 1] < numbers[i]) {
					int tmp = numbers[i + 1];
					numbers[i + 1] = numbers[i];
					numbers[i] = tmp;
					// number is switched next position
					numbersSwitched = true;
				}
			}
		} while (numbersSwitched);
		return numbers;
	}



public int[] bubbleSort2(int[] numbers) {
// Number is switched next position or not true or false
  int n = numbers.length;
  for (int i = n-1; i >= 1; i--) {
     for (int j = 1; j <= i; j++) {
                // number - 1 > number
       if (numbers[j - 1] > numbers[j]) {
	    int tmp = numbers[j - 1];
	    numbers[j - 1] = numbers[j];
	    numbers[j] = tmp;}
      }
 } 
return numbers;
}


	/*
	 * Insert Sorting Given a list l, and a new list nl for each element
	 * originallistelem in list l: for each element newlistelem in list nl: if
	 * (originallistelem < newlistelem): insert originallistelem in nl before
	 * newlistelem else move to the next element if originallistelem has not been
	 * inserted: insert at end of nl Implementation: Instertion algorithm is not
	 * fast algorithm,it uses nested loops to shift in the list it useful only small
	 * datasets. (o(n^2)
	 */
	public List<Integer> insertSort(List<Integer> numbers) {
		final List<Integer> sortedList = new LinkedList<>();
		originalList: for (Integer number : numbers) {
			for (int i = 0; i < sortedList.size(); i++) {
				if (number < sortedList.get(i)) {
					sortedList.add(i, number);
					continue originalList;
				}
			}
			sortedList.add(sortedList.size(), number);
		}
		return sortedList;
	}

//Quick Sort Algirithm
	/*
	 * method quicksort(list l): if l.size < 2: return l let pivot = l(0) let lower
	 * = new list let higher = new list for each element e in between l(0) and the
	 * end of the list: if e < pivot: add e to lower else add e to higher let
	 * sortedlower = quicksort(lower) let sortedhigher = quicksort(higher) return
	 * sortedlower + pivot + sortedhigher
	 */
	/**
	 * @author manjunath
	 * {@summary}:This algorithm is recursive. The base case is when a list is provided with zero or one element. You
	can simply return that list because it is already sorted.
	The second part of the algorithm is to pick an arbitrary element from the list, called the pivot. In
	this case the first element in the list was used, but any element could have been picked. The remain-
	ing elements are separated into two: those lower than the pivot, and those equal to or larger than
	the pivot.
	The method is then called on each of the two smaller lists, which will return in each segment being
	sorted. The final list is the smaller elements, now sorted, followed by the pivot, and then the higher
	sorted elements.
	 */
	public static List<Integer> quicksort(List<Integer> numbers) {
		if (numbers.size() < 2) {
			return numbers;
		}
		final Integer pivot = numbers.get(0);
		final List<Integer> lower = new ArrayList<>();
		final List<Integer> higher = new ArrayList<>();
		for (int i = 1; i < numbers.size(); i++) {
			if (numbers.get(i) < pivot) {
				lower.add(numbers.get(i));
			} else {
				higher.add(numbers.get(i));

			}
		}
		final List<Integer> sorted = quicksort(lower);
		sorted.add(pivot);
		sorted.addAll(quicksort(higher));
		return sorted;
	}

//Merge Sort
	public List<Integer> mergesort(final List<Integer> values) {
		if (values.size() < 2) {
			return values;
		}
		final List<Integer> leftHalf = values.subList(0, values.size() / 2);
		final List<Integer> rightHalf = values.subList(values.size() / 2, values.size());
		return merge(mergesort(leftHalf), mergesort(rightHalf));
	}

	private static List<Integer> merge(final List<Integer> left, final List<Integer> right) {
		int leftPtr = 0;
		int rightPtr = 0;
		final List<Integer> merged = new ArrayList<>(left.size() + right.size());
		while (leftPtr < left.size() && rightPtr < right.size()) {
			if (left.get(leftPtr) < right.get(rightPtr)) {
				merged.add(left.get(leftPtr));
				leftPtr++;
			} else {
				merged.add(right.get(rightPtr));
				rightPtr++;
			}
		}
		while (leftPtr < left.size()) {
			merged.add(left.get(leftPtr));
			leftPtr++;
		}
		while (rightPtr < right.size()) {
			merged.add(right.get(rightPtr));
			rightPtr++;
		}
		return merged;
	}

	/*
	 * Binary Search The beauty of this algorithm is that you use the property of
	 * the sorted list to your advantage. You can throw away many elements without
	 * even examining them, because you know they definitely cannot be equal to the
	 * given element. If you have a list with one million elements, you can find a
	 * given element in as little as twenty comparisons. This algorithm has the
	 * performance of O(n) .
	 */
	public  boolean binarySearch(final List<Integer> numbers, final Integer value) {
		if (numbers == null || numbers.isEmpty()) {
			return false;
		}
		final Integer comparison = numbers.get(numbers.size() / 2);
		if (value.equals(comparison)) {
			System.out.println("Value found in the list"+value);
			return true;
		}
		if (value < comparison) {
			return binarySearch(numbers.subList(0, numbers.size() / 2), value);
		} else {
			System.out.println("Value not found in the list"+value);
			return binarySearch(numbers.subList(numbers.size() / 2 + 1, numbers.size()), value);
			
		}
	}
	
	
	
	
//Swaping number with out temp 
public void swap(int x, int y) throws Throwable {

		System.out.println("Before Swapping\nx = " + x + "\ny = " + y);

		x = x + y;
		y = x - y;
		x = x - y;

		System.out.println("After Swapping\nx = " + x + "\ny = " + y);
	}



	
/*
*1. Find the smallest item x, in the range of [0...n−1]
*2. Swap x with the (n−1) th item
*3. Reduce n by 1 and go to Step 1
* the sorted list to your advantage. You can throw away many elements without
*/


 public int[] sortAscending(final int[] arr) {
	    for (int i = 0; i < arr.length - 1; i++) {
	        int minElementIndex = i;
	        for (int j = i + 1; j < arr.length; j++) {
	            if (arr[minElementIndex] >= arr[j]) {
	                minElementIndex = j;
	            }
	        }
	 
	        if (minElementIndex != i) {
	            int temp = arr[i];
	            arr[i] = arr[minElementIndex];
	            arr[minElementIndex] = temp;
	        }
	    }
		return arr;
         
	}

/*
*1. Find the largest item x, in the range of [0...n−1]
*2. Swap x with the (n−1) th item
*3. Reduce n by 1 and go to Step 1
* the sorted list to your advantage. You can throw away many elements without
*/
public  void sortDescending(final int[] arr) {
	    for (int i = 0; i < arr.length - 1; i++) {
	        int maxElementIndex = i;
	        for (int j = i + 1; j < arr.length; j++) {
	            if (arr[maxElementIndex] <= arr[j]) {
	                maxElementIndex = j;
	            }
	        }
	 
	        if (maxElementIndex != i) {
	            int temp = arr[i];
	            arr[i] = arr[maxElementIndex];
	            arr[maxElementIndex] = temp;
	        }
	    }
         
	}
//shift right concept
public void InsertionSort_shiftright(int[] arr) {
	for(int i=1;i<arr.length;i++) {
		int current = arr[i];
		int j = i-1;
		while(j >=0 && arr[j] > current) {
			arr[j+1] = arr[i];
			j--;
		}
		arr[j+1] = current;	
		}


}




}
