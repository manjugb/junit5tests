package javaMethods;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class swap {

	public void swaptemp(int x, int y) throws Throwable {
		int temp;
		System.out.println("Before Swapping\nx = " + x + "\ny = " + y);

		temp = x;
		x = y;
		y = temp;

		System.out.println("After Swapping\nx = " + x + "\ny = " + y);

	}

	public void swapwtemp(int x, int y) throws Throwable {

		System.out.println("Before Swapping\nx = " + x + "\ny = " + y);

		x = x + y;
		y = x - y;
		x = x - y;

		System.out.println("After Swapping\nx = " + x + "\ny = " + y);
	}
	
	public void getCount(String s) {
		Map<Character, Integer> charMap = new HashMap<Character, Integer>();
		System.out.println(s);
	    char[] arr = s.toCharArray();
	   System.out.println(arr);

	    for (char value: arr) {

	       if (Character.isAlphabetic(value)) {
	           if (charMap.containsKey(value)) {
	               charMap.put(value, charMap.get(value) + 1);

	           } else {
	               charMap.put(value, 1);
	           }
	       }
	    }

	    System.out.println(charMap);
	}

	public  void findDuplicate(String letter)
	{

	    for(int i=0; i<letter.length();i++)
	    {
	        for( int j=i+1; j<letter.length();j++)
	        {
	            if(letter.charAt(i)==letter.charAt(j))
	            {
	                System.out.println(letter.charAt(j));
	                break;
	            }
	        }
	    }
	}
	
	
	

}
