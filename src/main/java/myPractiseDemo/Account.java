package myPractiseDemo;

public class Account {
	private float balance;

	public float getBalance() {
		return balance;
	}

	/*
	 * public void setBalance(float balance) { if (balance > 0) { this.balance =
	 * balance; } else { System.out.println("Value is not a Positive Number "); }
	 * 
	 * }
	 */
	
	public void deposit(float amount) {
		//this.balance = this.balance + amount; //treaditional way
		if(amount > 0)
		balance += amount;
	}
	
	public void withdraw(float amount) {
		if(amount > 0) {
			balance -= amount;
		}
	}

}
