package cardgame.controller;

import java.util.ArrayList;

import cardgame.model.Deck;
import cardgame.model.Player;

class View {
	//this is constructor is still under development
	public void something() {}
	public void setController(GameController gc) {}
}
	
public class GameController{
	enum GameState{
		AddingPLayers,
		CardsDealt,
		WinnersRevealed;
	}
	
	Deck deck;
	ArrayList<Player> players;
	Player winner;
	View view;
	GameState gameState;
	
	public GameController(View view,Deck deck) {
		this.view = view;
		this.deck = deck;
		players = new ArrayList<Player>();
		gameState = GameState.AddingPLayers;
		view.setController(this);
	}
	
	public void run() {
		while (true) {
			switch (gameState) {
			case AddingPLayers:
				view.something();
				break;
			case CardsDealt:
				view.something();
			case WinnersRevealed:
				view.something();
				
				
			}
		}
	}
	
	public void addPlayer(String playerName) {
		if (gameState == GameState.AddingPLayers) {
			players.add(new Player(playerName));
			view.something();
		}
	}
	public void startGame() {
		if (gameState != GameState.AddingPLayers) {
			deck.shuffle();
			int playerIndex = 1;
			for(Player player : players) {
				player.addCardToHand(deck.removeTopCard());
				view.something();
			}
			gameState = GameState.CardsDealt;
		}
	}
 }
