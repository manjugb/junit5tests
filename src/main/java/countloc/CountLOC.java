package countloc;

public class CountLOC {
    public static int count(String text) {  
      	int count=0;
      	String data = new String(text);
       	data = data.replaceAll("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)","");
      	String[] stringArray = data.split("\\n+\\s*");
       	count = stringArray.length;
        return count;
    }
}