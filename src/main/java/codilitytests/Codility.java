package codilitytests;

import static util.constants.myLogger;

import java.util.HashMap;

public class Codility {


//first method binary gap
public static Integer findBinaryGap(String binVal) {
        Integer retVal = 0;
        String splitVal[] = binVal.split("1");
        int endVal = splitVal.length;
        if (binVal.endsWith("0")) {
            endVal = endVal - 1;
        }
        for (int incr = 0; incr < endVal; incr++) {
            if (retVal < splitVal[incr].length()) {
                retVal = splitVal[incr].length();
            }
        }
        return retVal;
    }

//Second Solution	
	
	public static int Solution(int N) {
        String binary = Integer.toBinaryString(N);
        int count = 0;
        int tmpCount = 0;
        for (int i = 0; i < binary.length(); i++) {
            if (binary.charAt(i) == '0') {
                if (i > 0 && binary.charAt(i - 1) == '1') {
                    tmpCount++;
                } else {
                    if (tmpCount > 0) tmpCount++;
                }
            } else if (binary.charAt(i) == '1') {
                if (tmpCount > 0 && tmpCount > count) {
                    count = tmpCount;
                    myLogger.info(count);
                }
                tmpCount = 0;
            }
        }
        return count;
     
    }
	
//Third Method
	public static int Solution1(int n) {
	    String binaryString = Integer.toBinaryString(n);
	    int count = 0, bigGap = 0, temptCount = 0;
	    for (int i = 0; i < binaryString.length(); i++) {
	        char c = binaryString.charAt(i);
	        if (c == '0') {
	            temptCount++;
	        } else {
	            count = temptCount;

	            if (count > bigGap) {
	                bigGap = count;
	            }
	            temptCount = 0;
	        }

	    }

	    return bigGap;
	}

	
// function to find the element occurring odd 
    // number of times 
	//first method
    public static int getOddOccurrence(int arr[], int arr_size) 
    { 
        int i; 
        for (i = 0; i < arr_size; i++) { 
            int count = 0; 
            for (int j = 0; j < arr_size; j++) { 
                if (arr[i] == arr[j]) 
                    count++; 
            } 
            if (count % 2 != 0) 
                return arr[i]; 
        } 
        return -1; 
    } 
   //Second Method
    public static int getOddOccurrence1(int arr[], int n) 
    { 
        HashMap<Integer,Integer> hmap = new HashMap<>(); 
          
        // Putting all elements into the HashMap 
        for(int i = 0; i < n; i++) 
        { 
            if(hmap.containsKey(arr[i])) 
            { 
                int val = hmap.get(arr[i]); 
                          
                // If array element is already present then 
                // increase the count of that element. 
                hmap.put(arr[i], val + 1);  
            } 
            else
                  
                // if array element is not present then put 
                // element into the HashMap and initialize  
                // the count to one. 
                hmap.put(arr[i], 1);  
        } 
  
        // Checking for odd occurrence of each element present 
        // in the HashMap  
        for(Integer a:hmap.keySet()) 
        { 
            if(hmap.get(a) % 2 != 0) 
                return a; 
        } 
        return -1; 
    } 
    
   //Third Method
    public static int getOddOccurrence2(int ar[], int ar_size)  
    { 
        int i; 
        int res = 0; 
        for (i = 0; i < ar_size; i++)  
        { 
            res = res ^ ar[i]; 
        } 
        return res; 
    } 
    
   //Cyclic Rotation:Rotate an array to the right by a given number of stepts
    public static void cyclicrotation(int arr[],int ar_size) {
    	//Rotate the given array by n times toward right    
        for(int i = 0; i < ar_size; i++){    
            int j, last;    
            //Stores the last element of array    
            last = arr[arr.length-1];    
            
            for(j = arr.length-1; j > 0; j--){    
                //Shift element of array by one    
                arr[j] = arr[j-1];    
            }    
            //Last element of array will be added to the start of array.    
            arr[0] = last;    
    }
     
      //Displays resulting array after rotation    
        System.out.println("Array after right rotation: ");    
        for(int i = 0; i< arr.length; i++){    
        	myLogger.info(arr[i] + " ");
              
        }    
}
//Count number of jumps from X to Y with out loop
    public int frogjump(int X, int Y, int D) {
    	int result = (Y-X) % D == 0? (Y-X) / D : (Y-X) /D +1;
    	myLogger.info("Finally moved:"+result);
    	return result;
    	}
    
}