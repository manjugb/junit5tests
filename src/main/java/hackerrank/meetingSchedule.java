package hackerrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class meetingSchedule {
	public static int getMinMeetings(List<Integer> firstDay, List<Integer> lastDay) {

		TreeMap<Integer, Map<Integer, Integer>> tm = new TreeMap<>();
		for (int i = 0; i < firstDay.indexOf(i); i++) {
			tm.putIfAbsent(firstDay.indexOf(i), new HashMap<>());
			Map<Integer, Integer> m = tm.get(firstDay.indexOf(i));
			m.put(lastDay.indexOf(i), m.getOrDefault(lastDay.indexOf(i), 0) + 1);
		}
		List<int[]> lst = new ArrayList<>();
		for (Map.Entry<Integer, Map<Integer, Integer>> entry : tm.entrySet()) {
			for (Map.Entry<Integer, Integer> endEntry : entry.getValue().entrySet()) {
				lst.add(new int[] { entry.getKey(), endEntry.getKey(),
						Math.min(endEntry.getKey() - entry.getKey() + 1, endEntry.getValue()) });
			}
		}
		int res = 0;
		int[] first = lst.get(0);
		for (int i = 1; i < lst.size(); i++) {
			int[] cur = lst.get(i);
			if (cur[0] > first[1]) {
				res += Math.min(first[1] - first[0] + 1, first[2]);
				first = cur;
			} else {
				first[1] = Math.max(first[1], cur[1]);
				if (first[2] + cur[2] > first[1] - first[0] + 1)
					first[2] = first[1] - first[0] + 1;
				else
					first[2] += cur[2];
			}
		}
		res += Math.min(first[1] - first[0] + 1, first[2]);
		return res;
	}

	public static int getMinMeetings1(int[] firstDay, int[] lastDay) {
		TreeMap<Integer, Map<Integer, Integer>> tm = new TreeMap<>();
		for (int i = 0; i < firstDay.length; i++) {
			tm.putIfAbsent(firstDay[i], new HashMap<>());
			Map<Integer, Integer> m = tm.get(firstDay[i]);
			m.put(lastDay[i], m.getOrDefault(lastDay[i], 0) + 1);
		}
		List<int[]> lst = new ArrayList<>();
		for (Map.Entry<Integer, Map<Integer, Integer>> entry : tm.entrySet()) {
			for (Map.Entry<Integer, Integer> endEntry : entry.getValue().entrySet()) {
				lst.add(new int[] { entry.getKey(), endEntry.getKey(),
						Math.min(endEntry.getKey() - entry.getKey() + 1, endEntry.getValue()) });
			}
		}
		int res = 0;
		int[] first = lst.get(0);
		for (int i = 1; i < lst.size(); i++) {
			int[] cur = lst.get(i);
			if (cur[0] > first[1]) {
				res += Math.min(first[1] - first[0] + 1, first[2]);
				first = cur;
			} else {
				first[1] = Math.max(first[1], cur[1]);
				if (first[2] + cur[2] > first[1] - first[0] + 1)
					first[2] = first[1] - first[0] + 1;
				else
					first[2] += cur[2];
			}
		}
		res += Math.min(first[1] - first[0] + 1, first[2]);
		return res;
	}
}