package hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class transactions {

	public static int getTransactions(int userId, int locationId, int netStart, int netEnd) {
		final String URL = "https://jsonmock.hackerrank.com/api/transactions/search?userId=" + userId;

		String content;
		Float retAmount = 0f;

		try {
			content = getJSONContent(URL);
			TransactionList tlByPage = new Gson().fromJson(content, TransactionList.class);
			System.out.println(tlByPage);

			retAmount += getAmountByNetStartAndEnd(tlByPage, locationId, netStart, netEnd);

			int totalPages = tlByPage.getTotal_pages();

			for (int i = 2; i <= totalPages; i++) {
				content = getJSONContent(URL + "&page=" + i);
				tlByPage = new Gson().fromJson(content, TransactionList.class);
				retAmount += getAmountByNetStartAndEnd(tlByPage, locationId, netStart, netEnd);
			}
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}
		return Math.round(retAmount);

	}

	private static Float getAmountByNetStartAndEnd(TransactionList tl, int locationID, int netStart, int netEnd) {
		Float retAmount = 0f;

		for (Transaction data : tl.getData()) {
			int ipData = Integer.parseInt(data.getIp().substring(0, data.getIp().indexOf(".")));
			String amountData = data.getAmount().replaceAll(",", "");
			if (data.getLocation().getId() == locationID && ipData >= netStart && ipData <= netEnd) {
				retAmount += Float.parseFloat(amountData.substring(amountData.indexOf("$") + 1));
			}
		}

		return retAmount;
	}

	private static String getJSONContent(String urlToFetch) throws IOException {
		final int OK = 200;
		URL url = new URL(urlToFetch);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		int responseCode = connection.getResponseCode();
		if (responseCode == OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();

			return response.toString();
		}

		return null;
	}

	private static class TransactionList {
		private int page;
		private int per_page;
		private int total;
		private int total_pages;
		private Transaction[] data;

		public int getPage() {
			return page;
		}

		public void setPage(int page) {
			this.page = page;
		}

		public int getPer_page() {
			return per_page;
		}

		public void setPer_page(int per_page) {
			this.per_page = per_page;
		}

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public int getTotal_pages() {
			return total_pages;
		}

		public void setTotal_pages(int total_pages) {
			this.total_pages = total_pages;
		}

		public Transaction[] getData() {
			return data;
		}

		public void setData(Transaction[] data) {
			this.data = data;
		}

		@Override
		public String toString() {
			return "TrasactionList{" + "page='" + page + '\'' + ", per_page='" + per_page + '\'' + ", total='" + total
					+ '\'' + ", total_pages='" + total_pages + '\'' + ", data=" + data + '}';
		}
	}

	private static class Transaction {
		private Integer id;
		private Integer userId;
		private String userName;
		private String timestamp;
		private String txnType;
		private String amount;
		private Location location;
		private String ip;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}

		public String getTxnType() {
			return txnType;
		}

		public void setTxnType(String txnType) {
			this.txnType = txnType;
		}

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		@Override
		public String toString() {
			return "Transaction{" + "id=" + id + ", userId=" + userId + ", userName='" + userName + '\''
					+ ", timestamp='" + timestamp + '\'' + ", txnType='" + txnType + '\'' + ", amount='" + amount + '\''
					+ ", location=" + location + ", ip='" + ip + '\'' + '}';
		}
	}

	private static class Location {
		private Integer id;
		private String address;
		private String city;
		private String zipcode;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getZipcode() {
			return zipcode;
		}

		public void setZipcode(String zipcode) {
			this.zipcode = zipcode;
		}

		@Override
		public String toString() {
			return "Location{" + "id=" + id + ", address='" + address + '\'' + ", city='" + city + '\'' + ", zipcode='"
					+ zipcode + '\'' + '}';
		}
	}
}
