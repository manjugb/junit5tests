package javainterview;

public class HomePage {
public static final String homepageResources;
public static String homepageLink="epam.com";

static {
	homepageResources="HOme page resources";
}

public static void openPage() {
	System.out.println(homepageLink);
}


public static class Runner{
	public static void main(String[] args) {
		HomePage.openPage();
	}

}
}
