package javainterview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class javainterviewques {
	/**
	 * @author manjunath
	 * @return
	 * @this class having java interview questions
	 */
	// interview questions related string and string buffer
	// what is the difference betwwen string and string buffer
	// do we have reverse function in as string is no
	// de we have reverse function in string buffer yes
	// using for loop dont have any reverse function for string since string is
	// immutable
	// charAt returns char at specified index.
	public String reverse(String s) {
		String rev = "";
		if (s != null && !s.isEmpty()) {
			int len = s.length();
			for (int i = len - 1; i >= 0; i--) {
				rev = rev + s.charAt(i);
			}
		}
		System.out.println("Reverse" + rev + "String");
		return rev;

	}

	// using String Buffer there is reverse function available since String buffer
	// is mutable

	public StringBuffer reverseWithStringBuffer(String s) {
		StringBuffer sf = new StringBuffer(s);
		// StringBuffer s1 = sf.reverse();
		// System.out.println(s1);

		return sf.reverse();
	}

	// Write a function remove special characters in string

	public String removeSpecialCharInString(String s) {

		if (s != null && !s.isEmpty()) {
			s = s.replaceAll("[^a-zA-Z0-9]", "");
		}

		return s;

	}
	// write reverse a number

	public long checkReverseOfNumber(long num) {
		long rev = 0;
		while (num != 0) {
			rev = rev * 10 + num % 10;
			num = num / 10;
		}
		System.out.println("Reverse of a Number:" + rev);
		return rev;
	}

	// reverse using string buffer

	public StringBuffer checkReverseOfNumberWithStringBuffer(long num) {
		StringBuffer revnum;
		revnum = new StringBuffer(String.valueOf(num)).reverse();
		return revnum;

	}

	// find the missing number in an array o(nxn)

	public long missingNumber(long[] arr) {
		long sum = 0;
		long sum1 = 0;
		long mnum;
		int arraySize;
		arraySize = arr.length;
		// sum of arry without missing number
		for (int i = 0; i < arraySize; i++) {
			sum = sum + arr[i];
		}
		System.out.println("Sum with out missing Number:" + sum);
		for (int j = 0; j <= arraySize + 1; j++) {
			sum1 = sum1 + j;
			System.out.println(sum1);
		}
		System.out.println("Sum with missing Number:" + sum1);
		mnum = sum1 - sum;
		return mnum;

	}

	// calculate missing number in n number o(2n)

	public static int findMissingN(int[] arr) {
		int sum = 0, max = 0;
		int mnum = 0;
		for (int n : arr) {
			sum += n;
			if (n > max)
				max = n;
		}
		mnum = (max * (max + 1) / 2) - sum;
		System.out.println("Missing Number:" + mnum);
		return mnum;
	}

	// find missing numbers

	public static ArrayList<Integer> findMissing4(ArrayList<Integer> arr) {
		int max = 0;
		ArrayList<Integer> misNums = new ArrayList();
		int[] neededNums;
		for (int n : arr) {
			if (n > max)
				max = n;
		}
		neededNums = new int[max];// zero for any needed num
		for (int n : arr) {// iterate again
			neededNums[n == max ? 0 : n]++;// add one - used as index in second array (convert max to zero)
		}
		for (int i = neededNums.length - 1; i > 0; i--) {
			if (neededNums[i] < 1)
				misNums.add(i);// if value is zero, than index is a missing number
		}
		return misNums;
	}

	// find duplicates in an array worst case o(nxn)

	public static String findDuplicates(String names[]) {
		int i = 0, j;
		if (names != null && names.length != 0) {
			for (i = 0; i < names.length; i++) {
				for (j = i + 1; j < names.length; j++) {
					if (names[i].equals(names[j])) {
						return names[i];

					}System.out.println("Duplicate Word:" + names[i]);
				}
			}
			
			
		} else {
			System.out.println("There were no duplicates");
		}
		return names[i];

	}

	// duplicates o(n) using hashSets

	public static String findDuplicatesHashSets(String[] names) {
		Set<String> store = new HashSet<String>();
		for (String name : names) {
			if (store.add(name) == false) {
				return name;
			}
			System.out.println("Found duplicate word:" + name);
		}
		return "\0";

	}

	// o(2n) time complexity

	public static String findDuplicatesHashMap(String[] names) {
		String sword = "";
		// Store values into hashMap
		Map<String, Integer> storeMap = new HashMap<String, Integer>();

		for (String name : names) {
			Integer count = storeMap.get(name);
			if (count == null) {
				storeMap.put(name, 1);
			} else {
				storeMap.put(name, ++count);
			}
		}

		// get the values hashmap objects entry set and gives key and value
		Set<Entry<String, Integer>> entrySet = storeMap.entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			if (entry.getValue() > 1) {
				System.out.println("duplicate element:" + entry.getKey() + "Value:" + entry.getValue());
				sword = entry.getKey();

				return sword;
			}
			// return entry.getKey();

		}
		return sword;

	}

	// time complexity =O(n)
	public static int[] smallLargestNuber(int[] numbers) {
		// create largest, smallest variables assign to zero
		int largest = numbers[0];
		int smallest = numbers[0];

		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] > largest) {
				largest = numbers[i];
			} else if (numbers[i] < smallest) {
				smallest = numbers[i];
			}

		}
		int[] numb = { largest, smallest };
		System.out.println("Largest Number is:" + largest);
		System.out.println("Smallest Number is:" + smallest);
		return numb;

	}
	
	public static void findDuplicate1(String input) {
        
        for (int i = 0; i < input.length(); i++) {

            for (int j = i + 1; j < input.length(); j++) {

                if (input.charAt(i) == input.charAt(j)) {
                    System.out.println("Duplicate is: " + input.charAt(i));
                }
            }
        }
    }

	public static String findDuplicate2_char(String ch) {
		int len = 0;
		int count = 0;
		do
        {  
          try
          {
          char name[]=ch.toCharArray();
              len=name.length;
              count=0;
              for(int i=0;i<len;i++)
               {
                  if((name[0]==name[i])&&((name[0]>=65&&name[0]<=91)||(name[0]>=97&&name[0]<=123))) 
                      count++;
               }
              if(count!=0)
                System.out.println(name[0]+" "+count+" Times");
              ch=ch.replace(""+name[0],"");   
              System.out.println(ch);
          }
          catch(Exception ex){}
        }
        while(len!=1);
		return ch;
   }
	
	public static Map<Character, Integer> finduplicatechar(String str) {
		Map<Character, Integer> charMap = new HashMap<Character, Integer>();
	    char[] arr = str.toCharArray();

	    for (char value: arr) {

	       if (Character.isAlphabetic(value)) {
	           if (charMap.containsKey(value)) {
	               charMap.put(value, charMap.get(value) + 1);

	           } else {
	               charMap.put(value, 1);
	           }
	       }
	    }
		return charMap;
	}
	
	
	//usins stream
	

}

	



