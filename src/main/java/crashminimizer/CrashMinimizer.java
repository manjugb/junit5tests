package crashminimizer;

import static util.constants.myLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class CrashMinimizer {
    
    /**
     * This function minimizes a crashing test case to
     * a single character that still causes the crash.
     *
     * @param String command - the command to execute the
     *                         program under test.
     *
     * @param String failingTestInputFilename - the path 
     *                to the file causing a crash in the 
     *                target program. The contents of 
     *                this file are to be minimized by 
     *                this function.
     *
     * @return String - the final, minimized version of the 
     *                failing test input file which still 
     *                causes a crash.
    */
   
    
    
    public static String minimize(String command, String failingInputFilename) throws FileNotFoundException, IOException {
            List<String> lines = Utilities.readFile(failingInputFilename);
            for (String line : lines) {
                if (line.contains("æ")) {
                	myLogger.info(line);
                    return "æ";
                }
                else {
                	return "↑";
                }
            }
			return "";
        }
}