package codingchallenges;

import static util.constants.myLogger;

import java.util.HashMap;
import java.util.Map;

public class codingChallenge {
	// find missing number in a sorted list
	public static int findFirst(int[] arr) {
		int l = 1; // assume that increment by one
		int r = arr.length; // array length
		myLogger.info(r);
		while (r - l > 1) {
			int middle = (r + l) / 2;
			if (arr[middle] > middle) {
				r = middle;
			}
			l = middle;
		}
		return r;
	}
	
//using for loop
	
	public static int getMissingNo(int a[], int n) 
    {
        int total = 1;
        for (int i = 2; i <= (n + 1); i++)
        {
            total += i;
            total -= a[i - 2];
        }
        return total;
    }

//Covert integer to binary
	public static String inttobinary(int n) {
        
        String x = ""; //declare variable for storing binary string
		
		while (n > 0) {

			int a = n % 2;
	//		System.out.println(a);
			x = a + x;
		//	System.out.println(x);
			n = n / 2;
			//System.out.println(n);

		}

		myLogger.info(x);
		return x;

	}
	
	//find count occurrence of character
	
	
	public void getCount(String s) {
		Map<Character, Integer> charMap = new HashMap<Character, Integer>();
		myLogger.info(s);
	    char[] arr = s.toCharArray();
	    myLogger.info(arr);

	    for (char value: arr) {

	       if (Character.isAlphabetic(value)) {
	           if (charMap.containsKey(value)) {
	               charMap.put(value, charMap.get(value) + 1);

	           } else {
	               charMap.put(value, 1);
	           }
	       }
	    }

	    myLogger.info(charMap);
	}

}
