package collections;

public class Employee implements Comparable<Employee>{
	public int id;
	public String name;
	public int salary;
	
	
	public void setId(int id) {
		this.id=id;
	}
	
	public String getName(String name) {
		return name;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public int getSalary(int salary) {
		return salary;
	}
	
	public void setSalary(int salary) {
		this.salary=salary;
	}
	
	@Override
	public String toString() {
		return "Customer [id=" + id + ", Name=" + name + ", Salary=" + salary + "]";
	}

	@Override
	public int compareTo(Employee o) {
	
		return this.salary=salary;
	}
	

}
